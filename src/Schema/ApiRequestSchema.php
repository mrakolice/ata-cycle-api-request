<?php


namespace Ata\Cycle\ApiRequest\Schema;


use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Helpers\ExtendedRequest;
use Cycle\Annotated\Exception\AnnotationException;
use Doctrine\Common\Annotations\AnnotationException as DoctrineException;
use Doctrine\Common\Annotations\AnnotationReader;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Spiral\Tokenizer\ClassesInterface;

/**
 * Берет аннотацию из модели и применяет ее к входящему реквесту
 * Наименования инклюдов\фильтров и тд должны соответствовать JSON REST API.
 * /entity?filter[field]=value
 * /entity?filter[front_route.rel.field]=value -- фильтр осн модели
 * /entity?filter[front_route][rel.field]=value -- фильтр зависимости
 * /entity?include=rel.rel,rel
 * /entity?sort=-field,rel.rel.field
 * /entity?fields[]=id,emai,rel.fild
 */
class ApiRequestSchema
{
    /**
     * @var ClassesInterface
     */
    private $locator;
    /**
     * @var AnnotationReader
     */
    private $reader;

    /**
     * @var array<ApiRequest>
     */
    protected $requests = [];

    public function __construct(
        ClassesInterface $locator,
        AnnotationReader $reader = null)
    {
        $this->locator = $locator;
        $this->reader = $reader ?? new AnnotationReader();

        $this->initRequests();
    }

    protected function initRequests()
    {
        foreach ($this->locator->getClasses() as $class) {
            try {
                /** @var ApiRequest $ann */
                $ann = $this->reader->getClassAnnotation($class, ApiRequest::class);
            } catch (DoctrineException $e) {
                throw new AnnotationException($e->getMessage(), $e->getCode(), $e);
            }

            if ($ann === null) {
                continue;
            }

            $this->requests[$class->name] = $ann->forRepository(resolve('cycle-db')->getRepository($class->name));
        }
    }

    public function getAnnotation(string $class): ApiRequest
    {
        return $this->requests[$class];
    }

    public function find(string $class, ?Request $request = null): Collection
    {
        $annotation = $this->getAnnotation($class);

        // mb throw exception here
        if (is_null($annotation)) {
            return collect();
        }

        // find annotation for this model and remove not containing fields
        return collect($annotation->processRequest(ExtendedRequest::fromRequest($request)));
    }

}
