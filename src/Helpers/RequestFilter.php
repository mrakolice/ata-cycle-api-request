<?php


namespace Ata\Cycle\ApiRequest\Helpers;


use Illuminate\Support\Str;

class RequestFilter
{
    /**
     * Наименование поля, по которому будут фильтровать
     */
    public $field = null;

    /**
     * Наименование отношения
     */
    public $relation = '';

    /**
     * Оператор. Может быть одним из значений, указанных в ключах конфига api_request.operators
     */
    public $operator = null;

    /**
     * Значение в фильтре
     */
    public $value = null;

    /**
     * Показывает, что используется фильтрация вложенных сущностей
     */
    public $isNested = false;

    /**
     * Показывает, что используется фильтр по умолчанию
     */
    public $default = false;


    // Эта функция нужна, чтобы превратить входящий параметр rel rel=>rel=>rel=>field=>value в rel=>rel=>rel=>rel=>field
    // Написана отвратительно
    private function createRelatedMap(string $key, $value)
    {
        $first_key = array_key_first($value);
        if (is_array($value[$first_key])) {
            $this->relation .= config('api_request.related_delimiter') . $first_key;
            $this->createRelatedMap($first_key, $value[$first_key]);
            return;
        }

        $this->value = $value[$first_key];
        list($this->field, $this->operator) = array_pad(explode(config('api_request.operator_delimiter'), $first_key), 2, null);
    }

    public function __construct(string $field, $filterValue)
    {
        // Если не массив, то просто определяем оператор и значение
        $this->value = $filterValue;
        list($this->field, $this->operator) = array_pad(explode(config('api_request.operator_delimiter'), $field), 2, null);

        // если массив, идем рекурсивно до конца, ищем оператор и значение
        if (is_array($filterValue)) {
            $this->isNested = true;
            $this->relation = $field;
            $this->createRelatedMap($field, $filterValue);
        }

        if (is_string($this->value) &&
            Str::contains($this->value, config('api_request.list_delimiter'))
        ) {
            $this->operator = $this->operator ?? config('api_request.default_list_operator');

            $this->value = explode(config('api_request.list_delimiter'), $this->value);
        }

        $this->default = is_null($this->operator);

        if ($this->default) {
            return;
        }

        collect(config('api_request.operators'))->each(function ($operator, $key) {
            if (in_array($this->operator, $operator['api'])) {
                $this->operator = $key;
                return false;
            }
        });
    }
}
