<?php


namespace Ata\Cycle\ApiRequest\Exceptions;

use Exception;

class CannotQueryThroughFilterException extends Exception
{

    private $filter;

    public function __construct($filter)
    {
        $this->filter = $filter;

        parent::__construct();
    }
}
