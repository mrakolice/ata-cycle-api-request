<?php


namespace Ata\Cycle\ApiRequest\Traits;


use Ata\Cycle\ApiRequest\Schema\ApiRequestSchema;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

trait RequestQuerying
{
    public static function forRequest(?Request $request = null): Collection
    {
        return resolve(ApiRequestSchema::class)->find(get_called_class(), $request);
    }
}
