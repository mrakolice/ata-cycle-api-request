<?php

declare(strict_types=1);


namespace Ata\Cycle\ApiRequest\Annotations;

use Ata\Cycle\ApiRequest\Exceptions\CannotFindFilterFieldException;
use Ata\Cycle\ApiRequest\Exceptions\CannotQueryThroughFilterException;
use Ata\Cycle\ApiRequest\Helpers\ExtendedRequest;
use Ata\Cycle\ApiRequest\Helpers\RequestFilter;
use Ata\Cycle\ApiRequest\Schema\ApiRequestSchema;
use Cycle\ORM\RepositoryInterface;
use Cycle\ORM\Select;
use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Target;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Str;
use Spiral\Database\Injection\Parameter;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 * @Attributes({
 *      @Attribute("fields", type="array<Ata\Cycle\ApiRequest\Annotations\RequestField>"),
 *      @Attribute("includes", type="array<string>"),
 * })
 */
class ApiRequest
{
    /** @var array<RequestField> */
    private $fields = [];

    /** @var array<string> */
    private $includes = [];
    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @var Select
     */
    private $results;

    /**
     * @param array $values
     */
    public function __construct(array $values)
    {
        foreach ($values as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return RequestField[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return string[]
     */
    public function getIncludes(): array
    {
        return $this->includes;
    }

    public function forRepository(RepositoryInterface $repository): ApiRequest
    {
        $this->repository = $repository;

        return $this;
    }

    public function startProcessing(): ApiRequest
    {
        $this->results = null;

        return $this;
    }

    public function processRequest(ExtendedRequest $request): iterable
    {
        return $this->startProcessing()->processFilter($request)->processIncludes($request)->processSort($request)->processFields($request)->compile();
    }

    public function processFilter(ExtendedRequest $request): ApiRequest
    {
        $this->results = $this->results ?? $this->repository->select();

        // get filter fields
        // filter parameters and get exact fields and exact filters
        // create where query
        $filters = new ArrayCollection();

        $request->filters()->each(function (RequestFilter $queryFilter, $key) use ($filters) {
            $relationName = null;
            $options = [];

            $field = $this->findField($queryFilter);

            if (is_null($queryFilter->operator)) {
                $queryFilter->operator = $field->default;
            }

            throw_if(!in_array($queryFilter->operator, $field->filters), CannotQueryThroughFilterException::class, $queryFilter->operator);

            // Для этих операторов необходимо оборачивать массив в этот объект
            if (in_array($queryFilter->operator, ['in', 'not_in'])) {
                $queryFilter->value = new Parameter($queryFilter->value);
            }

            if ($queryFilter->isNested) {
                $relationName = $queryFilter->relation;

                $options = [
                    'where' =>
                        [
                            $queryFilter->field => [
                                config('api_request.operators.' . $queryFilter->operator . '.db') => $queryFilter->value
                            ]
                        ],
                    'method' => Select\JoinableLoader::JOIN,
                ];
            } elseif (Str::contains($queryFilter->field, config('api_request.related_delimiter'))) {
                $relationName = implode('.', array_slice(explode(config('api_request.related_delimiter'), $queryFilter->field), 0, -1));
            }

            if (!is_null($relationName)) {
                $this->results = $this->results->load($relationName, $options);
                return;
            }

            $filters[$queryFilter->field] = [
                config('api_request.operators.' . $queryFilter->operator . '.db') => $queryFilter->value
            ];
        });
        dump($filters);
        if (!$filters->isEmpty()) {
            $this->results = $this->results->where($filters->toArray());
        }

        return $this;
    }

    public function processIncludes(ExtendedRequest $request): ApiRequest
    {

        return $this;
    }

    public function processSort(ExtendedRequest $request): ApiRequest
    {

        return $this;
    }

    public function processFields(ExtendedRequest $request): ApiRequest
    {
        return $this;
    }

    public function compile(): iterable
    {
        return $this->results->fetchAll();
    }

    /**
     * @param RequestFilter $queryFilter
     * @return RequestField
     * @throws \Throwable
     */
    private function findField(RequestFilter $queryFilter): RequestField
    {
        $annotation = $this;
        $field = null;

        foreach (explode(config('api_request.related_delimiter'), $queryFilter->field) as $fieldName) {
            $field = collect($annotation->getFields())
                ->first(function (RequestField $f) use ($queryFilter, $fieldName) {
                    return $f->name == $fieldName;
                });

            if (is_null($field)) {
                $annotation = resolve(ApiRequestSchema::class)->getAnnotation(
                    $annotation->getIncludes()[$fieldName]
                );
            }
        };

        throw_if(is_null($field), CannotFindFilterFieldException::class, $queryFilter->field);

        return $field;
    }
}
