<?php


namespace Ata\Cycle\ApiRequest\Annotations;

use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"PROPERTY", "ANNOTATION"})
 * @Attributes({
 *      @Attribute("name", type="string", required=True),
 *      @Attribute("filters", type="array<string>"),
 *      @Attribute("sort", type="bool"),
 *      @Attribute("default", type="string"),
 *      @Attribute("column", type="string"),
 * })
 */
class RequestField
{
    /** @var array<string> */
    public $filters = [];

    /** @var bool */
    public $sort = false;

    /** @var string */
    public $name;

    /** @var string */
    public $default;

    /** @var string */
    public $column;

    /**
     * @param array $values
     */
    public function __construct(array $values)
    {
        foreach ($values as $key => $value) {
            $this->$key = $value;
        }

        // enable all filters by default
        if (!array_key_exists('filters', $values)) {
            $this->filters = array_keys(config('api_request.operators'));
        }

        if (!array_key_exists('default', $values)){
            $this->default = config('api_request.default_filters.'.get_called_class());
        }

        if (!array_key_exists('column', $values)){
            $this->column = $this->name;
        }
    }
}
