<?php


namespace Ata\Cycle\ApiRequest\Tests\Unit;


use Ata\Cycle\ApiRequest\Tests\Models\TestModel;
use Ata\Cycle\ApiRequest\Tests\Models\TestRelatedModel;
use Ata\Cycle\ApiRequest\Tests\TestCase;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Http\Request;

class FilterTest extends TestCase
{
    public function testShouldFilterViaEntityField()
    {
        $testModel = new TestModel(['stringField' => 'value']);
        $testModel->save();

        // /users?filter[field]=value
        $request = new Request([
            'filter' => [
                'stringField' => 'value'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result->count());
        $this->assertEquals('value', $result[0]->stringField);
    }

    public function testShouldFilterViaRelatedEntityField()
    {
        $testModel = new TestModel(['stringField' => 'value']);
        $testModel->related->add(new TestRelatedModel(['integerField' => 2]));
        $testModel->save();

        // /users?filter[related.field]=value
        $request = new Request([
            'filter' => [
                'related.integerField' => 2
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result->count());
        $this->assertEquals(2, $result->last()->related[0]->integerField);
    }

    public function testShouldFilterRelatedEntity()
    {
        $testModel = new TestModel(['stringField' => 'value']);
        $testModel->related = new ArrayCollection([
                new TestRelatedModel(['integerField' => 2]),
                new TestRelatedModel(['integerField' => 1]),
                new TestRelatedModel(['integerField' => 3]),
            ]
        );

        $testModel->save();

        // /users?filter[related][field]=value
        $request = new Request([
            'filter' => [
                'related' => [
                    'integerField' => 2
                ]
            ]
        ]);

        $result = TestModel::forRequest($request);


        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result[0]->related->count());
        $this->assertEquals(2, $result[0]->related[0]->integerField);
    }

    public function testShouldFilterEqualsOnDefaultForNonStringFields()
    {
        (new TestModel(['integerField' => 3]))->save();
        (new TestModel(['integerField' => 2]))->save();

        // /users?filter[related][field]=value
        $request = new Request([
            'filter' => [
                'integerField' => 3
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);
        $this->assertEquals(1, $result->count());
        $this->assertEquals(3, $result[0]->integerField);
    }

    public function testShouldFilterLikeCaseInsensitiveOnDefaultForStringFields()
    {
        (new TestModel(['stringField' => 'vaLue']))->save();
        (new TestModel(['stringField' => 'vAlue']))->save();
        (new TestModel(['stringField' => 'other']))->save();

        // /users?filter[related][field]=value
        $request = new Request([
            'filter' => [
                'stringField' => 'value'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);
        $this->assertEquals(2, $result->count());
        $this->assertEquals('vaLue', $result[0]->stringField);
        $this->assertEquals('vAlue', $result[1]->stringField);
    }

    public function testShouldFilterEquals()
    {
        $testModel = new TestModel(['integerField' => 3]);
        $testModel->save();

        // /users?filter[field:eq]=value
        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);
        $this->assertEquals(3, $result[0]->integerField);
    }

    public function testShouldFilterNotEquals()
    {
        // /users?filter[field:neq]=value
        $testModel = new TestModel(['integerField' => 3]);
        $testModel->save();

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.not_equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertEmpty($result);

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.not_equals.api')[0] => 2
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integerField);
    }

    public function testShouldFilterGreaterThan()
    {
        // /users?filter[field:gt]=value
        $testModel = new TestModel(['integerField' => 3]);
        $testModel->save();

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.greater_than.api')[0] => 3
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertEmpty($result);

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.greater_than.api')[0] => 2
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integerField);
    }

    public function testShouldFilterGreaterThanEquals()
    {
        // /users?filter[field:gte]=value
        (new TestModel(['integerField' => 3]))->save();

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.greater_than_equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integerField);
    }

    public function testShouldFilterLesserThan()
    {
        // /users?filter[field:lt]=value
        (new TestModel(['integerField' => 3]))->save();

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.lesser_than.api')[0] => 3
            ]
        ]);

        $this->assertEmpty(TestModel::forRequest($request));

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.lesser_than.api')[0] => 4
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integerField);
    }

    public function testShouldFilterLesserThanEquals()
    {
        // /users?filter[field:lte]=value
        (new TestModel(['integerField' => 3]))->save();

        $request = new Request([
            'filter' => [
                'integerField:' . config('api_request.operators.lesser_than_equals.api')[0] => 3
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result[0]->integerField);
    }

    public function testShouldFilterLikeCaseSensitive()
    {
        // /users?filter[field:like]=value
        (new TestModel(['stringField' => 'VALUE']))->save();
        (new TestModel(['stringField' => 'value']))->save();

        $request = new Request([
            'filter' => [
                'stringField:' . config('api_request.operators.like_sensitive.api')[0] => 'value'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(1, $result->count());
        $this->assertEquals('value', $result[0]->stringField);
    }

    public function testShouldFilterLikeCaseInsensitive()
    {
        // /users?filter[field:ilike]=value
        (new TestModel(['stringField' => 'VALUE']))->save();
        (new TestModel(['stringField' => 'value']))->save();

        $request = new Request([
            'filter' => [
                'stringField:' . config('api_request.operators.like_insensitive.api')[0] => 'value'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('VALUE', $result[0]->stringField);
        $this->assertEquals('value', $result[1]->stringField);
    }

    public function testShouldFilterInOnDefaultWhenPassMultipleValues()
    {
        // /users?filter[field]=value1,value2
        (new TestModel(['stringField' => 'VALUE']))->save();
        (new TestModel(['stringField' => 'value']))->save();

        $request = new Request([
            'filter' => [
                'stringField' => 'value,VALUE'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('VALUE', $result[0]->stringField);
        $this->assertEquals('value', $result[1]->stringField);
    }

    public function testShouldFilterIn()
    {
        // /users?filter[field:in]=value,value2
        (new TestModel(['stringField' => 'VALUE']))->save();
        (new TestModel(['stringField' => 'value']))->save();

        $request = new Request([
            'filter' => [
                'stringField:' . config('api_request.operators.in.api')[0] => 'value,VALUE'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('VALUE', $result[0]->stringField);
        $this->assertEquals('value', $result[1]->stringField);
    }

    public function testShouldFilterNotIn()
    {
        // /users?filter[field:not-in]=value1,value2
        (new TestModel(['stringField' => 'VALUE']))->save();
        (new TestModel(['stringField' => 'value']))->save();
        (new TestModel(['stringField' => 'other']))->save();

        $request = new Request([
            'filter' => [
                'stringField:' . config('api_request.operators.not_in.api')[0] => 'value,VALUE'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(1, $result->count());
        $this->assertEquals('other', $result[0]->stringField);
    }

    public function testShouldFilterBetween()
    {
        // /users?filter[field:beetween]=value1,value2
        (new TestModel(['stringField' => 'value0']))->save();
        (new TestModel(['stringField' => 'value1']))->save();
        (new TestModel(['stringField' => 'value2']))->save();
        (new TestModel(['stringField' => 'value3']))->save();
        (new TestModel(['stringField' => 'value4']))->save();

        $request = new Request([
            'filter' => [
                'stringField:' . config('api_request.operators.between.api')[0] => 'value1,value3'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(3, $result->count());
        $this->assertEquals('value1', $result[0]->stringField);
        $this->assertEquals('value2', $result[1]->stringField);
        $this->assertEquals('value3', $result[2]->stringField);
    }

    public function testShouldFilterNotBetween()
    {
        // /users?filter[field:not-beetween]=value1,value2
        (new TestModel(['stringField' => 'value0']))->save();
        (new TestModel(['stringField' => 'value1']))->save();
        (new TestModel(['stringField' => 'value2']))->save();
        (new TestModel(['stringField' => 'value3']))->save();
        (new TestModel(['stringField' => 'value4']))->save();

        $request = new Request([
            'filter' => [
                'stringField:' . config('api_request.operators.not_between.api')[0] => 'value1,value3'
            ]
        ]);

        $result = TestModel::forRequest($request);

        $this->assertNotEmpty($result);

        $this->assertEquals(2, $result->count());
        $this->assertEquals('value0', $result[0]->stringField);
        $this->assertEquals('value4', $result[1]->stringField);
    }
}
