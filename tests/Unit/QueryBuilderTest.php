<?php


namespace Ata\Cycle\ApiRequest\Tests\Unit;



use Ata\Cycle\ApiRequest\Tests\TestCase;

class QueryBuilderTest extends TestCase
{
    public function testShouldFilterViaEntityField(){
        // /users?filter[field]=value
        $this->assertTrue(false);
    }

    public function testShouldFilterViaRelatedEntityField(){
        $this->assertTrue(false);
    }

    public function testShouldFilterRelatedEntity(){
        $this->assertTrue(false);
    }

    public function testShouldIncludeRelatedEntities(){
        $this->assertTrue(false);
    }

    public function testShouldSortViaEntityField(){
        $this->assertTrue(false);
    }

    public function testShouldSortViaRelatedEntityField(){
        $this->assertTrue(false);
    }

    public function testShouldSortRelatedEntity(){
        $this->assertTrue(false);
    }

    public function testShouldSelectEntityFields(){
        $this->assertTrue(false);
    }

    public function testShouldSelectRelatedEntityFields(){
        $this->assertTrue(false);
    }

}
