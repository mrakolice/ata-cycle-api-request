<?php


namespace Ata\Cycle\ApiRequest\Tests;


use Ata\Cycle\Tests\BaseTestCase;

class TestCase extends BaseTestCase
{

    protected function getEnvironmentSetUp($app)
    {
        $requestConfig = include __DIR__ . '/../config/api_request.php';
        $app['config']->set('api_request', $requestConfig);

        parent::getEnvironmentSetUp($app);
    }

    protected function getPackageProviders($app)
    {
        return array_merge(
           parent::getPackageProviders($app),
            [
                'Ata\Cycle\ApiRequest\PackageServiceProvider'
            ]
        );
    }
}
