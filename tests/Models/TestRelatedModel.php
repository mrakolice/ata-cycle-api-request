<?php



namespace Ata\Cycle\ApiRequest\Tests\Models;


use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Annotations\RequestField;
use Ata\Cycle\ApiRequest\Annotations\StringRequestField;
use Ata\Cycle\Models\CycleModel;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Table;
use Ata\Cycle\Mappers\DefaultMapper;

/**
 * @Entity(mapper=DefaultMapper::class)
 * @Table(columns={
 *      "booleanField": @Column(type="boolean", nullable=true),
 *      "integerField": @Column(type="integer", nullable=true),
 *      "stringField": @Column(type="string", nullable=true),
 *      "decimalField": @Column(type="decimal(2,0)", nullable=true),
 *      "datetimeField": @Column(type="datetime", nullable=true),
 *      "jsonField": @Column(type="json", nullable=true)
 *    })
 * @ApiRequest(
 *     fields={
 *          @StringRequestField(name="stringField"),
 *          @RequestField(name="integerField"),
 *     }
 * )
 */
class TestRelatedModel
{
    use CycleModel;

    public function __construct(array $data = [])
    {
        $this->__setData($data);
    }
}
