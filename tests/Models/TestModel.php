<?php


namespace Ata\Cycle\ApiRequest\Tests\Models;


use Ata\Cycle\ApiRequest\Annotations\ApiRequest;
use Ata\Cycle\ApiRequest\Annotations\RequestField;
use Ata\Cycle\ApiRequest\Annotations\StringRequestField;
use Ata\Cycle\ApiRequest\Traits\RequestQuerying;
use Ata\Cycle\Mappers\DefaultMapper;
use Ata\Cycle\Models\CycleModel;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Table;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity(mapper=DefaultMapper::class)
 * @Table(columns={
 *      "booleanField": @Column(type="boolean", nullable=true),
 *      "integerField": @Column(type="integer", nullable=true),
 *      "stringField": @Column(type="string", nullable=true),
 *      "decimalField": @Column(type="decimal(2,0)", nullable=true),
 *      "datetimeField": @Column(type="datetime", nullable=true),
 *      "jsonField": @Column(type="json", nullable=true)
 *    })
 * @ApiRequest(
 *     fields={
 *          @StringRequestField(name="stringField"),
 *          @RequestField(name="integerField"),
 *          @RequestField(name="includeField", column="related.integerField"),
 *     },
 *     includes={"related":TestRelatedModel::class}
 * )
 */
class TestModel
{
    use CycleModel;
    use RequestQuerying;

    public function __construct(array $data = [])
    {
        if (!array_key_exists('related', $data)) $data['related'] = new ArrayCollection();

        $this->__setData($data);
    }

    /**
     * @HasMany(target=TestRelatedModel::class)
    */
    public $related;
}
