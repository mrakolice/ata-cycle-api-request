FROM php:7.3-fpm-alpine AS base

LABEL Description="Ata Cycle ORM project"
LABEL maintainer="Svyatoslav Postevoy <postevoy.svyatoslav@gmail.com>"

WORKDIR /app/

# Создаем пользователя
ENV USER=docker-user
ARG UID=1000
ARG GID=1000
RUN if grep -q $GID /etc/group; then \
        GROUP_NAME=$(getent group $GID | cut -d: -f1); \
    else \
        addgroup --gid "$GID" "$USER" && GROUP_NAME=$USER; \
    fi \
 && adduser \
    --home "/home/docker-user" \
    --ingroup "$GROUP_NAME" \
    --disabled-password \
    --uid "$UID" \
    --gecos "" \
    "$USER"
RUN chown -R docker-user /app

# Устанавливаем пакеты
RUN apk update && apk --no-cache add \
    postgresql-dev icu-dev \
    autoconf make g++ gcc git \
    ghostscript libtool ffmpeg \
    jpegoptim optipng pngquant \
    freetype libpng freetype-dev \
    gifsicle libpng-dev libzip-dev \
    imagemagick-dev libjpeg-turbo-dev \
    bash bash-doc bash-completion

# Расширения PHP
RUN docker-php-ext-configure gd \
    --with-gd \
    --with-freetype-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ \
 && docker-php-ext-install -j$(nproc) \
    iconv intl mbstring bcmath pdo \
    pdo_pgsql opcache zip pcntl exif gd


# Устанавливаем composer и ускоряем его с помощью prestissimo
RUN curl -sS https://getcomposer.org/installer | php \
 && mv composer.phar /usr/local/bin/composer \
 && su docker-user -c 'composer global require hirak/prestissimo --no-plugins --no-scripts'

# Фикс iconv библиотеки в alphine
RUN apk add --no-cache \
    --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ \
    --allow-untrusted gnu-libiconv
ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

# Забираем скрипты
#COPY ./docker/scripts/entrypoint.sh /tmp/scripts/
#RUN chmod +x /tmp/scripts/entrypoint.sh

# Устанавливаем Xdebug
RUN pecl install xdebug && \
    docker-php-ext-enable xdebug
# Переменная окружения необходимая для Xdebug-модуля PhpStorm
ENV PHP_IDE_CONFIG="serverName=AtaCycleOrm"

# Чистим кэш пакетов
RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/pear

# Прокидываем настройки php-fpm
COPY .docker/php-fpm/www.conf /usr/local/etc/php-fpm.d/
COPY .docker/php-fpm/php.ini /usr/local/etc/php/conf.d/
COPY .docker/php-fpm/xdebug.ini /usr/local/etc/php/conf.d/

FROM base

USER ${UID}

# Устанавливаем авторизационный токен композера
COPY auth.json auth.json
# Устанавливаем зависимоси composer'а
COPY composer.json composer.json
#RUN su docker-user && composer install --prefer-dist --no-scripts --no-dev --no-autoloader

# Забираем проект в контейнер
COPY ./ /app/
USER root

# Завершаем установку зависимостей composer'а
RUN su docker-user && composer dump-autoload --no-scripts --no-dev --optimize

#ENV COMPOSER_MEMORY_LIMIT -1

RUN mkdir -p /home/docker-user/.config/psysh \
 && touch /home/docker-user/.bash_history /home/docker-user/.config/psysh/psysh_history
RUN chown -R docker-user /home/docker-user

ENV PS1="\e[1;36m\]\u@docker\e[0;37m\]:\e[1;34m\]\w\e[0;37m\]$ \e[0;37m\]"

EXPOSE 80
